jest.mock('./../src/logProcessor/index.js', () => require('./../__mocks__/logProcessor'));
jest.useFakeTimers();

export default () => modules.processor;
export const setConfig = (c) => (config = c);

import * as Logger from './../src/index.js'
import * as logProcessor from './../src/logProcessor';

describe('Log Processor', () => {

  beforeEach(() => {
    logProcessor.resetMock();
  });

  it('should be able to set config in ', () => {
    Logger.config('My Custom Config');
    expect(logProcessor.getConfig()).toBe('My Custom Config')
  });

  describe('Instant Events', () => {
    it('should be able to create an instance event with default level', () => {
      Logger.event('My Custom Event');
      expect(logProcessor.getLastLog()).toMatchObject({
        data: 'My Custom Event',
        type: 1,
        meta: { level: 3 },
      });
    });
  });

  describe('Duration Events', () => {
    it('should be able to create duration event', async () => {
      let eventEnd = Logger.durationEvent('Event Start');
      setTimeout(() => {
        eventEnd('End Event');
      }, 1000);

      jest.runTimersToTime(500);
      expect(logProcessor.getLastLog()).not.toBeTruthy();
      jest.runTimersToTime(1000);
      expect(logProcessor.getLastLog()).toMatchObject({
        data: { start: 'Event Start', end: 'End Event' },
        type: 4,
        meta: { level: 3 }
      })
    });
  });
});