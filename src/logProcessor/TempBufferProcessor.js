/**
 * A tiny, quick log process that just stores the log in a buffer before handing it off to a 
 * processor that may be loaded later. This is typically used at startup, so that heavy JS does 
 * not have to be parsed during startup time
 */

let rawLogs = [];
const process = async ({ data, type, meta, timestamp }) => rawLogs.push({ data, type, meta, timestamp });
const getUnprocessedLogs = async () => rawLogs;

export default (config) => ({ process, getUnprocessedLogs });
