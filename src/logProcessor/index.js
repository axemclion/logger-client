const modules = {
  processor: null
};
let config = {};

/**
 * Function to dunamically inject a log processor
 * Typically used to replace the quickStart processor with a processor that does real work like sending to server
 * Also can be used by developers if they wanna just inspect logs, or override default behaviour
 */
export const injectProcessor = async (processor) => {
  // First, immediatly switch to new processor, so that new logs are handled by this
  let oldProcessor = modules.processor;
  modules.processor = processor;

  // Next, pick up unprocessed logs from the old processor and hand them to the new processor
  if (oldProcessor !== null) {
    let unprocessedLogs = await oldProcessor.getUnprocessedLogs();
    unprocessedLogs.forEach(log => modules.processor.process(log));
  }
};

// At startup, inject the light weight processor
import lightProcessor from 'logger-processor-startup';
modules.processor = lightProcessor(config);

// Then eventually load the larger, more heavy weight processor
import(/* webpackChunkName: "heavy-log-processor" */ 'logger-processor-heavy').then(async (heavyProcessor) => {
  //HACK ES6 dynamic imports dont seem to work with export default :(, hence using default keyword
  let processor = await heavyProcessor.default(config);
  injectProcessor(processor);
});

export default () => modules.processor;
export const setConfig = (c) => (config = c);
