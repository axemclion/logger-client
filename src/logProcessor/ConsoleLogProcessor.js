/**
 * A client side only logger, used for testing and doing console.log
 */

const process = async ({ data, type, meta, timestamp }) => {
  console.log(`${new Date(timestamp.time).toTimeString()} : [${meta.level || ''}] ${meta.name || '-'} `, data);
}
const getUnprocessedLogs = async () => [];

export default async (config) => {
  await new Promise(resolve => setTimeout(resolve, 5000)); // Pretend link this processor takes time to setup
  return { process, getUnprocessedLogs };
};
