require('babel-polyfill');

import * as _ from './constants';
import { TYPE } from './../../util/constants';
import { load, getUnprocessedLogs, recordEvent, recordPartialEvent } from 'logger-storage';

import Replicator from 'replicator';
const replicator = new Replicator();


self.addEventListener('message', ({ data: { payload, type } }) => {
  handlers[type](payload)
});

const handlers = {
  async[_.TYPE_MAIN_LOAD](config) {
    await load(config);
    self.postMessage({ type: _.TYPE_WORKER_LOADED });
  },
  async[_.TYPE_MAIN_PROCESS](payload) {
    let event = replicator.decode(payload);
    if (event.type === TYPE.INSTANT || event.type === TYPE.DURATION) {
      recordEvent(event);
    } else if (event.type === TYPE.START || event.type === TYPE.END) {
      recordPartialEvent(event.meta.name, event);
    }
  },
  async[_.TYPE_MAIN_UNLOAD]() {
    let unprocessedLogs = await getUnprocessedLogs();
    self.postMessage({ type: _.TYPE_WORKER_UNLOADED, payload: unprocessedLogs });
  }
}