export const TYPE_WORKER_LOADED = '0.1';
export const TYPE_WORKER_UNLOADED = '0.3';
export const TYPE_WORKER_DONE_PROCESS = '0.2';

export const TYPE_MAIN_PROCESS = '1.2';
export const TYPE_MAIN_LOAD = '1.3';
export const TYPE_MAIN_UNLOAD = '1.4';