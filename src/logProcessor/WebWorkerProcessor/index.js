import * as _ from './constants';
import { requestIdleCallback } from './../../util/requestIdleCallback';

import Replicator from 'replicator';
const replicator = new Replicator();

let Worker = require("worker-loader?name=worker.js!./worker.js");
let worker = Worker();

worker.onmessage = ({ data: { payload, type } }) => handlers[type](payload);

const handlers = {
  [_.TYPE_WORKER_LOADED]() { },
  [_.TYPE_WORKER_UNLOADED]() { },
  [_.TYPE_WORKER_DONE_PROCESS](payload) {
    console.log(payload);
  }
}

/**
 * Process logs when the browser is free. It serializes the messages, which could be an expensive operation
 * Thats why this is run on requestIdleCallback
 */
const process = async (event) => requestIdleCallback(() => {
  worker.postMessage({
    type: _.TYPE_MAIN_PROCESS,
    payload: replicator.encode(event)
  });
});

/**
 * Called when a new log processor replaces this guy
 * This is async as it may take a while to wind down.
 */
const getUnprocessedLogs = async () => new Promise((resolve, reject) => {
  handlers[_.TYPE_WORKER_UNLOADED] = (events) => {
    handlers[_.TYPE_WORKER_UNLOADED] = () => { };
    resolve(events);
  };
  worker.postMessage({ type: _.TYPE_MAIN_UNLOAD });
});

/**
 * Returns when the web worker side of this processor is also loaded
 * All log processors export a single function that return an instance of the 
 * log processor when it is ready
 */
export default async (config) => new Promise((resolve, reject) => {
  handlers[_.TYPE_WORKER_LOADED] = () => {
    handlers[_.TYPE_WORKER_LOADED] = () => { };
    resolve({ process, getUnprocessedLogs });
  };
  worker.postMessage({ type: _.TYPE_MAIN_LOAD, payload: config });
});
