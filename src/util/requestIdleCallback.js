const ric = fn => {
  if (typeof requestIdleCallback === 'function') {
    return requestIdleCallback(fn);
  } else {
    return setTimeout(fn, 1);
  }
};

const cic = handle => {
  if (typeof cancelIdleCallback === 'function') {
    return cancelIdleCallback(handle);
  } else {
    return cancelTimeout(handle);
  }
}

export { ric as requestIdleCallback, cic as cancelIdleCallback };