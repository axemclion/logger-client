export default function () {
  return {
    time: new Date().getTime()
  }
}

export const compareTimeStamps = (a, b) => a.time < b.time;