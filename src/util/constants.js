export const LEVEL = { DEBUG: 1, LOG: 2, INFO: 3, WARN: 4, ERROR: 5 };
export const TYPE = { INSTANT: 1, START: 2, END: 3, DURATION: 4 };