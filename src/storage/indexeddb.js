import { TYPE } from './../util/constants'

const VERSION = 1;
const DBNAME = 'client-logger';
const TABLE_EVENTS = 'events';
const TABLE_PARTIAL_EVENTS = 'partial_events';

const STATUS_READY = 'readyToSend';
const STATUS_SENDING = 'sending';
const STATUS_DONE_SENDING = 'done_sending';
const STATUS_FAILED = 'failed_sending';

const INDEX_STATUS = 'status';

const ACCESS_READ_WRITE = 'readwrite';
const ACCESS_READ = 'read';

const TIMEOUT = 5000;

let indexedDB = self.indexedDB || self.mozIndexedDB || self.webkitIndexedDB || self.msIndexedDB;

import * as transport from 'logger-transport';

let canSchedule = true;
/**
 * Tha Main Scheduler, responsible for sending events to server, deleted events that are done, etc
 */
const scheduleSendingEvents = async () => {
  let eventUpdates = await transport.ready();
  // Setting status rather than deleting them since deletion is more efficient in a cursor
  // This also enables alternate eviction stategies
  await updateStatus(eventUpdates.done, STATUS_DONE_SENDING);
  // For failed events, lets simply reset the status that they are ready to send
  await updateStatus(eventUpdates.fail, STATUS_READY);

  // Now prune the events that are successful  
  // TODO - Make this less eager, don't run on every request
  await iterateEventsByStatus(STATUS_DONE_SENDING, cursor => { cursor.delete(); });

  // Start sending next set of events
  await iterateEventsByStatus(STATUS_READY, cursor => {
    cursor.update({ ...cursor.value, status: STATUS_SENDING });
    // This needs to be sync since IndexedDB transaction will terminate if this 
    // is an async function. Hence, cannot use iterators or streams here
    // To use iterators or streams, we will have to manually buffer events, too much work for now
    return transport.sendEvent(cursor.primaryKey, cursor.value.event);
  });

  if (canSchedule) { setTimeout(() => scheduleSendingEvents(), TIMEOUT); }
};

// Start of functions that "Storage" provider exports

/**
 * Lets the main thread know when this is ready
 */
export async function load(config) {
  let db = await openDB(DBNAME, VERSION);
  // Replace all "sending status" to failed, since we will no longer get this data back
  await iterateEventsByStatus(STATUS_SENDING, cursor => {
    cursor.update({ ...cursor.value.event, status: STATUS_READY });
  });
  await transport.load(config);
  scheduleSendingEvents();
}

/**
 * Returns all logs that are in the local storeage, but not yet processed
 * Also a way to start unloading this log processor
 */
export async function getUnprocessedLogs() {
  // TODO - Look up the databases to get unprocessed logs
  let db = await openDB(DBNAME, VERSION);
  canSchedule = false;   // Stop any more scheduling of events
  db.close();
  return [];
}

/**
 * Records instant and duration events. Just save them as quickly as possible in the storage
 * We can process them or send them in a separate function
 */
export function recordEvent(event) {
  return new Promise(async (resolve, reject) => {
    let db = await openDB(DBNAME, VERSION);
    try {
      let transaction = db.transaction([TABLE_EVENTS], ACCESS_READ_WRITE);
      let req = transaction.objectStore(TABLE_EVENTS).add({
        event,
        [INDEX_STATUS]: STATUS_READY
      });
      req.onsuccess = (e) => resolve(e.target.result);
      req.onerror = (e) => reject(e);
    } catch (e) {
      reject(e);
    }
  });
}

/**
 * Process individual start and stop events
 */
export function recordPartialEvent(name, event) {
  return new Promise(async (resolve, reject) => {
    let db = await openDB(DBNAME, VERSION);
    try {
      let transaction = db.transaction([TABLE_EVENTS, TABLE_PARTIAL_EVENTS], ACCESS_READ_WRITE);
      let partialEventsTable = transaction.objectStore(TABLE_PARTIAL_EVENTS);
      let eventsTable = transaction.objectStore(TABLE_EVENTS);
      let req = partialEventsTable.get(name);
      req.onsuccess = (e) => {
        let eventStack = e.target.result || [];
        if (event.type === TYPE.START) {
          // This is a start event, so just save it
          eventStack.push(event);
          let req = partialEventsTable.put(eventStack.sort((a, b) => a.timestamp.time > b.timestamp.time), name);
          req.onsuccess = (e) => resolve(e.target.result);
        } else if (event.type === TYPE.END) {
          // This is a stop event. Look if there is a corresponding start. 
          let result = eventStack.pop();
          if (result) {
            // IF there is a corresponding start, save the start stop pair as a full event
            let req = partialEventsTable.put(eventStack, name);
            req.onsuccess = (e) => {
              let putReq = eventsTable.add({
                event: combinePartialEvents(result, event),
                [INDEX_STATUS]: STATUS_READY
              });
              putReq.onsuccess = (e) => resolve(e.target.result);
              putReq.onerror = reject;
            }
            req.onerror = (e) => reject(e);
          } else {
            resolve(result);
          }
        }
      };
      req.onerror = (e) => reject(e);
    } catch (e) {
      reject(e);
    }
  });
}

// Start of helper functions

/**
 * Check if there is already a connection established. If yes, use that
 * Else create a new one. Using only one connection since connections are expensive
 * and once connection can have multiple transactions
 */
let dbConnection = null;
function openDB(name, version) {
  return new Promise((resolve, reject) => {
    if (dbConnection !== null) {
      resolve(dbConnection);
    }
    let req = indexedDB.open(name, version);
    req.onsuccess = (e) => resolve(dbConnection = e.target.result)
    req.onupgradeneeded = (e) => {
      dbConnection = e.target.result;
      try {
        let eventsTable = dbConnection.createObjectStore(TABLE_EVENTS, { autoIncrement: true });
        eventsTable.createIndex(INDEX_STATUS, INDEX_STATUS, { unique: false });
        dbConnection.createObjectStore(TABLE_PARTIAL_EVENTS, {});
        // This is a version change transaction, so close this and re-open the database
        dbConnection = null;
      } catch (e) {
        reject(e);
      }
    };
    req.onerror = (e) => reject(e);
  });
}

const combinePartialEvents = (startEvent, endEvent) => ({
  data: { start: startEvent.data, end: endEvent.data },
  meta: { ...startEvent.meta, ...endEvent.meta },
  timestamp: { start: startEvent.timestamp, end: endEvent.timestamp },
  type: TYPE.DURATION
});

const updateStatus = (keys = [], status) => new Promise(async (resolve, reject) => {
  let db = await openDB(DBNAME, VERSION);
  let store = db.transaction([TABLE_EVENTS], ACCESS_READ_WRITE).objectStore(TABLE_EVENTS);
  // Update status in same transaction
  (function update(i) {
    if (i >= keys.length) {
      resolve(i); return;
    }
    let req = store.get(keys[i]);
    req.onsuccess = (e) => {
      let data = { ...e.target.result, status: status };
      let putReq = store.put(data, keys[i]);
      putReq.onsuccess = (e) => update(i + 1);
      putReq.onerror = reject;
    };
    req.onerror = reject;
  })(0);
});

const iterateEventsByStatus = (status, cb) => new Promise(async (resolve, reject) => {
  let db = await openDB(DBNAME, VERSION);
  let req = db.transaction([TABLE_EVENTS], ACCESS_READ_WRITE).objectStore(TABLE_EVENTS).index(INDEX_STATUS).openCursor(status);
  req.onsuccess = (e) => (e.target.result && cb(e.target.result) !== false) ? e.target.result.continue() : resolve();
  req.onerror = reject;
});