const BATCH_SIZE = 50;
let config = {};

import Replicator from 'replicator';
const replicator = new Replicator();

let FormData = require('./html5-formdata-polyfilll');

// Start of Exports 

/**
 * Do any loading that may be required to establish connection with server
 * In case of WebSocket transport, this would typically be about setting up
 * the web socket connection
 */
export const load = async (c) => { config = c };

let eventsToSend = [];

export const ready = () => new Promise(async (resolve, reject) => {
  let events = await httpRequest(eventsToSend);
  eventsToSend = [];
  resolve(events);
});

/**
 * Synchronous method to send event. In case of HTTP transport, it simply buffers the events and sends them
 * later.
 */
export const sendEvent = (key, event) => {
  eventsToSend.push({ key, event });
  return eventsToSend.length < BATCH_SIZE;
};

// TODO - Replace mock HTTP request with a real request
const httpRequest = (events = []) => new Promise((resolve, reject) => {
  if (events.length === 0) { resolve([]); return; }

  let formData = new FormData();
  formData.append('logs', new Blob([replicator.encode(events)], { type: "text/plain" }));

  var xhr = new XMLHttpRequest();
  xhr.open('POST', config.server, true);
  xhr.onreadystatechange = () => resolve({ [xhr.status === 200 ? 'done' : 'fail']: events.map(event => event.key) });
  xhr.send(formData);
});
