require('babel-polyfill');

import TimeStamp from './util/timestamp';
import getProcessor, { injectProcessor, setConfig } from './logProcessor'
import { LEVEL, TYPE } from './util/constants';

export * from './util/constants';

/**
 * A way to inject a custom processor. Exposed so that devs could 
 * change the logging method if needed during development
 */
export const _injectProcessor = injectProcessor;

/**
 * A way to configure various options for the logger
 * At the least, the options object should have a server key to indicate
 * where the logs have to be sent
 */
export function config(options){
  setConfig(options);
}

/**
 * Main logger function, called by all other helper functions
 * @param data - Data to be logged. Duration events have start and end keys
 * @param type - Enumeration from Logger.TYPE. One of [INSTANT | START | END]
 * @param meta - Metadata for the event. Logging Levels like INFO or DEBUG is one metadata
 * @param timestamp - The timestamp object. For instant events, this just has a single time, duration events have start and end. 
 */
export function record(data, type = TYPE.INSTANT, meta = {}, timestamp = TimeStamp()) {
  getProcessor().process({ data, type, meta, timestamp });
}

/**
 * Log an event that occurs at a single time instant
 * @param data - Data to be logger
 * @param level - metadata about logging level
 */
export const event = (data, level = LEVEL.INFO) => record(data, TYPE.INSTANT, { level });

/**
 * Start logging an event that has a duration and will last over a short period of time
 * If an event lasts for a longer period and you don't want to hold on to a reference or memory,
 * use Logger.start and Logger.end
 * @param {*} start - Data to be logged at the start. Will be available as data.start in the event
 * @param {Number} level - metadata about logging level
 * @return {Function} - A function that should be called, signifying end of event
 */
export const durationEvent = (startData, level = LEVEL.INFO, startTime = TimeStamp()) => (
  endData, endTime = TimeStamp()
) => record(
  { start: startData, end: endData }, TYPE.DURATION, { level }, { start: startTime, end: endTime }
);

/**
 * Logs the start of an event with the given name. If there is already events with this name started, 
 * the first end event call will correspond to this event. i.e. duration events using this are nested
 * If there is no corresponding end call, the event is ignored
 * To avoid ambiguity, for shorter durations or for overlapping events use Logger.durationEvent 
 * @param {string} name - Name of the even that is started logging
 * @param {*} data - Data to be logged with the event
 * @param {Number} level - metadata about logging level
 */
export const start = (name, data, level = LEVEL.INFO) => record(data, TYPE.START, { level, name });

/**
 * Logs the end of an event with a given name. If no events are started with this name, the event is ignored
 * If there are multiple events started, this method signifies end of the last event that started - 
 * i.e. these events are nested 
 * To avoid ambiguity, for shorter durations or for overlapping events use Logger.durationEvent 
 * @param {string} name - Name of the even that is started logging
 * @param {*} data - Data to be logged with the event
 * @param {Number} level - metadata about logging level
 */
export const end = (name, data) => record(data, TYPE.END, { name });


// Setting up Helper functions like Logger.debug, Logger.info, etc.
const logAtLevel = level => (...data) => { record(data, TYPE.INSTANT, { level }) }

export const debug = logAtLevel(LEVEL.DEBUG);
export const log = logAtLevel(LEVEL.LOG);
export const info = logAtLevel(LEVEL.INFO);
export const warn = logAtLevel(LEVEL.WARN);
export const error = logAtLevel(LEVEL.ERROR);

