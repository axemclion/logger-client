var Replicator = require('replicator');
var replicator = new Replicator();
var Busboy = require('busboy');
var StringDecoder = require('string_decoder').StringDecoder;
var decoder = new StringDecoder('utf8');
var chalk = require('chalk');

module.exports = function (app) {
  app.post('/recordLogs', function (req, res) {

    var busboy = new Busboy({ headers: req.headers });

    busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
      if (fieldname === 'logs') {
        var content = [];
        file.on('data', function (data) {
          content.push(decoder.write(data));
        });
        file.on('end', function () {
          var logs = replicator.decode(content.join(''));
          logs.forEach(function (log) {
            try {
              //console.log(log);
              let res = [chalk.blue(log.key)];
              if (log.event.type === 1) {
                res.push(chalk.yellow(new Date(log.event.timestamp.time).toTimeString()));
                res.push(JSON.stringify(log.event.data))
              } else if (log.event.type === 4) {
                res.push(chalk.yellow(new Date(log.event.timestamp.start.time).toTimeString()))
                res.push(chalk.yellow(new Date(log.event.timestamp.end.time).toTimeString()))
                res.push(JSON.stringify(log.event.data.start))
                res.push(JSON.stringify(log.event.data.end))
              } 
              res.push(chalk.green(log.event.meta.name || ''));
              console.log(res.join(' '));
            } catch (e) { console.log(chalk.red(e)) }
          });
        });
      }
    });

    busboy.on('finish', function () {
      console.log(chalk.black.bgRed.bold('==============================='));
      res.send('OK');
    });
    req.pipe(busboy);
  });
};