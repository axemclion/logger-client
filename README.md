# Simple Client Logger

A simple client side logger to send events and other logs to a remote server. 

## Overview

By default, the client logger consists of three files - 

1. The main file that is included and loaded on application startup. This is a lightweight log collector that exposes the logger APIs. This simply records logs in memory till the other 2 files are loaded. 
2. A slightly heavier log processor that depends on a web worker. It is responsible for serializing the logs to send them to the web worker. The default, in-memor log collector is replaced by this file eventually after the application is started. This is done for better startup performance.
3. A web woker where most of the heavy lifting happens. The logs are temporarily stores in IndexedDB and then sent off to a server. The server can accept logs as if it was a multi-part file upload. Running all this on a worker ensure that the main thread stays available for business logic. 

## Running the example

1. Install dependencies using `npm install`
2. Start webpack dev server using `npm start`. 
3. Open example webpage at http://localhost:8080/
4. See the logs collected on the server in the `npm start` command's terminal

## Building the library and Usage

To build the library, run `npm run build`. Output is available in `./dist` folder. Include the file `logger.js` in the HTML file. This file also loads `worker.js` and `heavy-log-processor.js` dynamically, so ensure that those files are located at a relative `dist/` to the web page. To change this, edit `outputPath` in `webpack.config.js` and build the bundles again. 
 
## API

When included in a web page, a global variable called `Logger` is available. Look at `./src/index` for jsdoc like documentation on the individual API.