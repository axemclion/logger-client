var path = require('path');


var webJSConfig = {
  entry: {
    logger: path.join(__dirname, 'src/index.js')
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    pathinfo: true,
    publicPath: 'dist/',
    chunkFilename: "[name].chunk.js",
    filename: '[name].js',
    library: 'Logger',
    libraryTarget: 'umd'
  },
  module: {
    rules: [
      {
        test: /\.(js)$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  },
  devtool: 'source-map',
  devServer: {
    setup: require('./example/server/recordLogs.js'),
    contentBase: [path.join(__dirname, 'example/web')],
    compress: true,
    port: 8080
  },
  resolve: {
    alias: {
      'logger-processor-startup': path.resolve(__dirname, 'src/logProcessor/TempBufferProcessor.js'),
      //'logger-processor-heavy': path.resolve(__dirname, 'src/logProcessor/ConsoleLogProcessor.js'),
      'logger-processor-heavy': path.resolve(__dirname, 'src/logProcessor/WebWorkerProcessor/index.js'),
      'logger-storage': path.resolve(__dirname, 'src/storage/indexeddb.js'),
      'logger-transport': path.resolve(__dirname, 'src/transport/http.js')
    }
  }
};

module.exports = [webJSConfig];