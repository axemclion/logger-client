# Client Side Logger
Implement a client side logging library which allows application developers to log UI events to a remote server, or persist log entries to disk if the client is offline, for later dispatch when the client comes online.

# Guidelines

The implementation should, at a minimum:

1. Provide a client side API for developers to log information about UI interactions. The library should fulfill the Logging Specification provided below. The Sample Logging Use Case provides a specific example of the type of UI interaction the library would need to handle. Consider how the implementation would extend to other UI interactions.
2. Dispatch the log entries generated to a server. The server doesn't need to be anything more than a simple endpoint which the client can hit. Don’t need to do anything with the payload. 
3. If the client is offline, persist the log entries locally. 
4. Provide some client code which shows the library working. Show library code running, and how a developer using the library would access, configure and use it. 

# Logging Specification

1. There are two general types of events that we are concerned with capturing. Those that happen at a specific moment, and those that are active over a period of time. All events can have some optional data associated with them. Generally, the logger should capture the following information for each event:

  - The type of the event being logged
  - The time the event occurred
  - The period of time the active event spanned, if applicable
  - Optionally, a data payload associated with the event

2. Add any other fields which you think may be valuable keep track of, for the event.
3. In addition to the above information, also capture any events that were active at the time another event occurred.
4. Expect an average user interaction to generate 100s of logged events within a 5 minute period.

# Sample Logging Use Case

Capture the user’s interactions with the application’s Search UI.

The events to capture

- The amount of time spent interacting with the Search view in the app
- The amount of time the user is focused on the query input field in Search
- Each time a key is pressed in the input field
- The amount of time between submitting a query and displaying results