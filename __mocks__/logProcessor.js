let config = null;
let lastLog = null;
export default () => ({
  process(event) {
    lastLog = event;
  },

  getUnprocessedLogs() {

  }
});

export const setConfig = (c) => (config = c);
export const injectProcessor = () => { };

export const getConfig = () => config;
export const getLastLog = () => lastLog;
export const resetMock = () => { config = null; lastLog = null };